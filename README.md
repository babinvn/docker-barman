# Docker Barman
PostgreSQL Backup And Recovery Manager

## Building

```
docker build -t barman .
```

## Using

Configuration example:

```
version: '2'

networks:
  intranet:
    external: true

services:
  postgres:
    restart: unless-stopped
    image: postgres:latest
    container_name: postgres
    networks:
      - intranet
    ports:
      - 5432:5432
    environment:
      POSTGRES_PASSWORD: <password>
    volumes:
      - /var/docker/postgres/data:/var/lib/postgresql/data

  barman:
    restart: unless-stopped
    image: barman
    container_name: barman
    networks:
      - intranet
    environment:
      BARMAN_CRONTAB: "30 3 * * *" # GMT
      BARMAN_PASSWORD: <password>
      BARMAN_POLICY: "RECOVERY WINDOW OF 7 DAYS"
    volumes:
      - /var/docker/barman/archives:/var/lib/barman
      - /var/docker/barman/restore:/restore

```

Environment variable `BARMAN_CRONTAB` defines crontab schedule for database
backup while switch log is scheduled to 30-minute period:

```
0,30 * * * * barman switch-xlog --force --archive postgres
${BARMAN_CRONTAB} barman backup postgres
```

For barman to connect to PostgreSQL server create `barman`
and `streaming_barman` users:

```
docker exec -u postgres -it postgres createuser -s barman

docker exec -u postgres -it postgres psql -c "alter user barman with password '<password>'"

docker exec -u postgres -it postgres createuser --replication streaming_barman

docker exec -u postgres -it postgres psql -c "alter user streaming_barman with password '<password>'"
```

To enable streaming:

```
echo "host replication streaming_barman all md5" >> /var/docker/postgres/data/pg_hba.conf

docker restart postgres
```

To check:

```
docker exec -u barman -it barman barman check postgres
```

To list backups:

```
docker exec -u barman -it barman barman list-backup postgres
```

To restore:

```
docker exec -u barman -it barman barman recover postgres latest /restore
```

Check you backups regularly!

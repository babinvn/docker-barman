FROM debian:buster

RUN apt-get update \
  && apt-get install -y cron curl wget rsync gnupg2 tini \
  && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
  && . /etc/os-release \
  && echo "deb http://apt.postgresql.org/pub/repos/apt/ $VERSION_CODENAME-pgdg main" | tee /etc/apt/sources.list.d/pgdg.list \
  && curl https://dl.2ndquadrant.com/default/release/get/deb | bash \
  && apt-get update \
  && apt-get install -y postgresql-12 postgresql-client-12 \
  && apt-get install -y barman \
  && apt-get clean autoclean \
  && mkdir -p /home/barman \
  && chown -R barman:barman /home/barman \
  && usermod -d /home/barman barman

ADD docker-entrypoint.sh /
ADD .pgpass /opt/
ADD postgres.conf /opt/

ENV \
  BARMAN_CRONTAB="30 3 * * *" \
  BARMAN_PASSWORD="pazzword" \
  BARMAN_POLICY="RECOVERY WINDOW OF 2 WEEKS"

VOLUME [ "/var/lib/barman", "/restore" ]

ENTRYPOINT [ "/usr/bin/tini", "--", "/docker-entrypoint.sh" ]

CMD [ "/usr/sbin/cron", "-f", "-l", "0" ]

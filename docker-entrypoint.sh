#!/bin/sh
set -e

BARMAN_HOME="/home/barman"
BARMAN_ETC="/etc/barman.d"

if [ ! -f ${BARMAN_HOME}/.pgpass ]; then
  cp /opt/.pgpass ${BARMAN_HOME}/ &&
  sed -i "s|{BARMAN_PASSWORD}|${BARMAN_PASSWORD}|" ${BARMAN_HOME}/.pgpass &&
  chmod 0600 ${BARMAN_HOME}/.pgpass &&
  chown barman:barman ${BARMAN_HOME}/.pgpass
fi

if [ ! -f ${BARMAN_ETC}/postgres.conf ]; then
  cp /opt/postgres.conf ${BARMAN_ETC}/postgres.conf &&
  sed -i "s|{BARMAN_POLICY}|${BARMAN_POLICY}|" ${BARMAN_ETC}/postgres.conf &&
  chmod +r ${BARMAN_ETC}/postgres.conf
fi

chown -R barman:barman /home/barman
chown -R barman:barman /etc/barman.d
chown -R barman:barman /var/lib/barman
chown -R barman:barman /restore

cat << EOF > /var/spool/cron/crontabs/barman
0,30 * * * * barman switch-xlog --force --archive postgres
${BARMAN_CRONTAB} barman backup postgres
EOF

chmod 0600 /var/spool/cron/crontabs/barman
chown barman:crontab /var/spool/cron/crontabs/barman

exec "$@"
